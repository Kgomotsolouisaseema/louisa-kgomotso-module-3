import 'package:flutter/material.dart';

class Dashboard extends StatelessWidget {
  Dashboard({Key key}) : super(key: key);
  var products = getProducts();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dashboard"),
        actions: [
          InkWell(
            child: Padding(
              padding: EdgeInsets.all(15),
              child: Icon(Icons.settings),
            ),
            onTap: (() => {
                  Navigator.of(context).pushNamed("/edit_profile"),
                }),
          )
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            searchBar(),
            Expanded(
              child: GridView.count(
                primary: false,
                padding: const EdgeInsets.all(20.0),
                crossAxisSpacing: 10.0,
                mainAxisSpacing: 10,
                // Create a grid with 2 columns. If you change the scrollDirection to
                // horizontal, this produces 2 rows.
                crossAxisCount: 2,
                children: List.generate(products.length, (index) {
                  return Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      image: DecorationImage(
                        alignment: Alignment.topCenter,
                        fit: BoxFit.fill,
                        image: NetworkImage(products[index].imgUrl),
                      ),
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                          blurRadius: 2.0,
                          color: Colors.black87.withOpacity(0.05),
                        ),
                      ],
                    ),
                    child: Column(children: [
                      SizedBox(
                        height: 150,
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white10,
                              borderRadius: BorderRadius.circular(8)),
                          child: Text(
                            products[index].productName,
                            style: TextStyle(color: Colors.black),
                          ),
                        ),
                      ),
                    ]),
                  );
                }),
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (() => {Navigator.of(context).pushNamed("/profile")}),
        tooltip: 'Profile',
        child: Icon(Icons.supervised_user_circle),
      ),
    );
  }
}

Widget searchBar() {
  /// Search Bar
  return Container(
    margin: EdgeInsets.symmetric(horizontal: 12),
    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 14),
    decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(8),
      boxShadow: <BoxShadow>[
        BoxShadow(
          offset: Offset(5.0, 5.0),
          blurRadius: 5.0,
          color: Colors.black87.withOpacity(0.05),
        ),
      ],
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 9),
          child: Text(
            "Search",
            style: TextStyle(color: Color(0xff9B9B9B), fontSize: 17),
          ),
        ),
        Spacer(),
        Icon(Icons.search),
      ],
    ),
  );
}

List<ProductModel> getProducts() {
  List<ProductModel> products = new List();
  ProductModel productModel = new ProductModel();
  //1
  productModel.productName = "Poduct 1";
  productModel.imgUrl =
      "https://www.veggiesdontbite.com/wp-content/uploads/2020/04/vegan-salad-in-a-jar-21.jpg";
  productModel.priceInDollars = 20;
  products.add(productModel);
  productModel = new ProductModel();
  //1
  productModel.productName = "Product 2";
  productModel.imgUrl =
      "https://cleanfoodcrush.com/wp-content/uploads/2020/01/CleanFoodCrush-Taco-Mason-Jar-Salads-for-Meal-Prep.jpg";
  productModel.priceInDollars = 20;
  products.add(productModel);
  productModel = new ProductModel();
  //1
  productModel.productName = "Product 3";
  productModel.imgUrl =
      "https://gimmedelicious.com/wp-content/uploads/2017/04/mason-jar-3.jpg";
  productModel.priceInDollars = 20;
  products.add(productModel);
  productModel = new ProductModel();
  //1
  productModel.productName = "Product 4";
  productModel.imgUrl =
      "https://post.greatist.com/wp-content/uploads/sites/2/2019/05/noodle20cup_0.jpg";
  productModel.priceInDollars = 20;
  products.add(productModel);
  productModel = new ProductModel();
  //1
  productModel.productName = "Product 5";
  productModel.imgUrl =
      "https://i0.wp.com/post.greatist.com/wp-content/uploads/sites/2/2019/05/Physical20Kitchness_Mason20Jar20Sweet20Potato20Ground20Turkey20Scramble.jpg?w=1155&h=1776";
  productModel.priceInDollars = 20;
  products.add(productModel);
  productModel = new ProductModel();
  //1
  productModel.productName = "Product 6";
  productModel.imgUrl =
      "https://www.evolvingtable.com/wp-content/uploads/2016/12/Week-3-Salads.jpg";
  productModel.priceInDollars = 57;
  products.add(productModel);
  productModel = new ProductModel();
  return products;
}

class ProductModel {
  int priceInDollars; // price
  String productName; // name of the product
  String imgUrl; // product image url
  ProductModel({this.productName, this.imgUrl, this.priceInDollars});
}
