import 'package:flutter/material.dart';

import 'home.dart';
import 'login.dart';
import 'register.dart';
import 'dashboard.dart';
import 'profile.dart';
import 'edit_profile.dart';
import 'utils.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fat Chilli',
      theme: ThemeData(
        primarySwatch: primaryColor,
      ),
      home: Home(),
      routes: {
        '/': (context) => Home(),
        '/login': (context) => Login(),
        '/register': (context) => Register(),
        '/dashboard': (context) => Dashboard(),
        '/profile': (context) => Profile(),
        '/edit_profile': (context) => EditProfile()
      },
    );
  }
}
